import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:ngo/screens/splash.dart';
import 'package:ngo/utils/AppTheme.dart';
import 'package:ngo/utils/Application.dart';
import 'package:ngo/utils/app_router.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {
  MainApp() {
    final router = Router();
    AppRouter.defineRoutes(router);
    Application.router = router;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppTheme.getTheme(),
      onGenerateRoute: Application.router.generator,
      home: Splash(),
    );
  }
}
