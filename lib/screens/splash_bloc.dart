import 'package:rxdart/rxdart.dart';

class SplashBloc {
  Observable<bool> timer;
  Sink<void> onCompleted;

  SplashBloc() {
    timer = Observable<bool>.periodic(Duration(milliseconds: 5)).take(1);

    //timer.listen(getValue);
  }

  void getValue(bool event) {
    onCompleted.add(null);
  }
}
