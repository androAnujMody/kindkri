import 'package:flutter/material.dart';
import 'package:ngo/utils/Application.dart';
import 'package:ngo/utils/colors.dart';

class Splash extends StatelessWidget {
  BuildContext context;

  Splash() {
    Future.delayed(Duration(seconds: 5), () {
      Application.router.navigateTo(context, 'login', clearStack: true);
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: <Widget>[_splashImage(), _saying()],
    ));
  }

  _splashImage() {
    return Image(
      image: AssetImage('images/gandhi.jpeg'),
      fit: BoxFit.fill,
      width: double.infinity,
      height: double.infinity,
    );
  }

  _saying() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.black54,
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Center(
              child: Text(
                'You must not lose faith in humanity. '
                    'Humanity is an ocean; if a few drops of the ocean are dirty, '
                    'the ocean does not become dirty.',
                softWrap: true,
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(color: AppColors.white),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: CircularProgressIndicator(),
          )
        ],
      ),
    );
  }
}
