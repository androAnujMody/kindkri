import 'package:flutter/material.dart';

class AppColors {
  static const white = Colors.white;
  static const primary = Color(0xFF03A9F4);
  static const primaryDark = Color(0xFF0288D1);
  static const accent = Color(0xFF536DFE);
  static const primaryText = Color(0xFF212121);
}
