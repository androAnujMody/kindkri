import 'package:fluro/fluro.dart';
import 'package:ngo/main.dart';
import 'package:ngo/screens/login.dart';

class AppRouter {
  static String root = "/";
  static String login = "/login";

  static defineRoutes(Router router) {
    router.define(root, handler: Handler(handlerFunc: (context, params) {
      return MainApp();
    }));
    router.define(login, handler: Handler(handlerFunc: (context, params) {
      return Login();
    }));
  }
}
