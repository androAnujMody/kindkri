import 'package:flutter/material.dart';
import 'package:ngo/utils/colors.dart';

class AppTheme {
  static ThemeData getTheme() {
    final ThemeData base = ThemeData.light();
    return base.copyWith(
        primaryColor: AppColors.primary,
        primaryColorDark: AppColors.primaryDark,
        accentColor: AppColors.accent,
        scaffoldBackgroundColor: AppColors.white,
        textTheme: base.textTheme.copyWith(title: TextStyle(fontSize: 30.0)));
  }
}
